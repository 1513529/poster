# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 15:57:27 2018

@author: Administrator
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 15:52:43 2018

@author: Administrator
"""
import numpy as np 
import pandas as pd 

dataset=pd.read_csv("C:/Users/Administrator/Desktop/Poster/Well_Data_1.csv")

label=dataset.iloc[0:5339,[5]]
data=dataset.iloc[0:5339,[1,2,3,4]]

#filling NA values
#data=(data.fillna(0)) 
#testdat=testdat.fillna(0)

train_data=dataset.iloc[0:3999,[1,2,3,4]]
train_label=dataset.iloc[0:3999,5]
test_data=dataset.iloc[4000:5338,[1,2,3,4]]
test_label=dataset.iloc[4000:5338,5]

#train_data,test_data,train_labels,test_labels=train_test_split(data,label,test_size=20)

from sklearn import neighbors
clf=neighbors.KNeighborsClassifier(n_neighbors = 10, p = 2,weights = 'distance')
clf.fit(train_data, train_label)
pred_KNN=clf.predict(test_data)
from sklearn.metrics import accuracy_score
print("Accuracy of KNN: %.2f %%" %(100*accuracy_score(test_label, pred_KNN)))

from sklearn.svm import SVC   
svc = SVC(kernel = 'linear', C = 300)
svc.fit(train_data, train_label)
pred_Kernel_linear = svc.predict(test_data)
print("Accuracy of Kernel_linear: %.2f %%" %(100*accuracy_score(test_label, pred_Kernel_linear)))

from sklearn.neural_network import MLPClassifier
mlp = MLPClassifier(hidden_layer_sizes=(5),solver='sgd',learning_rate_init=0.0001,max_iter=1000)
mlp.fit(train_data, train_label)
pred_ANN=mlp.predict(test_data)
print("Accuracy of ANN: %.2f %%" %(100*accuracy_score(test_label, pred_ANN)))
import pandas as pd
pd.DataFrame(mlp.loss_curve_).plot()
